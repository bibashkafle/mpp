package project.main;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import project.ui.Forms;


public class Main extends Application {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch( args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		//Parent root = FXMLLoader.load(getClass().getResource("../ui/forms/LoginForm.fxml"));
		Parent root = FXMLLoader.load(getClass().getResource(Forms.LOGIN));
		primaryStage.setTitle("Library Management System");
		primaryStage.setScene(new Scene(root));
		primaryStage.setResizable(false);
		Platform.setImplicitExit(false);
		primaryStage.show();
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent event) {
		        Platform.exit();
		    }
		});
	}
}
