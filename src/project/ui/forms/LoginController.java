package project.ui.forms;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import project.business.SystemUser;
import project.dataaccess.DataAccess;
import project.ui.Forms;
import project.utils.Utilities;

public class LoginController {
	    
	@FXML private TextField userName;
	@FXML private PasswordField password;
	
	    @FXML protected void btnLogin_click(ActionEvent event) {
	    	
	    	SystemUser su = new SystemUser();
	    	su = su.Login(userName.getText(), password.getText());
	        
	    	if(su != null){
	    		userName.getScene().getWindow().hide();
				new Utilities().openDashboardWindow(Forms.HOMEPAGE,"Library Management System",su.getRole().toString());
	    	} 
	    	else{
	    		System.out.println("Invalid user");
	    	}
	    }
	   
	    @FXML protected void btnCancel_click(ActionEvent event) {
	    	//Platform.exit();
	    	
	    	System.out.println(new DataAccess().Read("SystemUser"));
	    }
	    
}