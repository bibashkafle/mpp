package project.ui.forms;



import java.util.List;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import project.business.Book;
import project.dataaccess.DataAccess;
import project.ui.Forms;
import project.utils.Utilities;


public class HomePageController  {
	@FXML
	private Button btnCheckOutBook;
	@FXML
	private Button btnAddMember;
	@FXML
	private Button btnEditMember;
	@FXML
	private Button btnAddBook;
	@FXML
	private Button btnAddBookCopy;
	@FXML
	private Button btnRecord;




	@FXML
	protected void showRecords(ActionEvent event){ //list of books not implemented
		DataAccess db = new DataAccess();
		List<Book> list = db.Read("Book");
		
		System.out.println("\n No no books");
		
		
		List<Book> copy = db.Read("CopyOfBooks");
		
		System.out.println(list);
		System.out.println(copy);
	}

	
	@FXML
	public void addMember() {
		try {
			new Utilities().openDashboardWindow(Forms.ADDMEMBER,"Member Add Form",null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void editMember() {
		try {
			new Utilities().openDashboardWindow(Forms.EDITMEMBER,"Member Edit Form",null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@FXML
	public void addBook() {
		try {
			new Utilities().openDashboardWindow(Forms.ADDBOOK,"Book Add Form",null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void addBookCopy() {
		try {
			new Utilities().openDashboardWindow(Forms.ADDBOOKCOPY,"Book Copy Add Form",null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void checkOutBook() {
		try {
			new Utilities().openDashboardWindow(Forms.CHECKINOUT,"CheckInOut Book",null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
