package project.ui.forms;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import project.business.Administrator;

public class AddBookController {

	//ADD BOOK
		//Administrator admin = new Administrator();
		@FXML
		private TextArea bio;

		@FXML
		private TextField title;

		@FXML
		private TextField Isbn;

		@FXML
		private TextField noOfBook;

		@FXML
		private TextField fname;

		@FXML
		private TextField lname;

		@FXML
		private TextField phone;

		@FXML
		private TextField street;

		@FXML
		private TextField city;

		@FXML
		private TextField state;

		@FXML
		private TextField zip;

		@FXML
		private CheckBox  cred;

		public void addBook_click(ActionEvent event){

			System.out.println("you click this "+fname.getText());
/*			int i = Integer.parseInt(Isbn.getText());
			int no=Integer.parseInt(noOfBook.getText());
			int z=Integer.parseInt( zip.getText());*/
			new Administrator().addBook(title.getText(), Isbn.getText(), noOfBook.getText(), fname.getText(), lname.getText(), phone.getText(), street.getText(), city.getText(), state.getText(), zip.getText() ,cred.isSelected(), bio.getText());
		}

		public void cancleBook_click(ActionEvent event){
			title.setText("");
			Isbn.setText("");
			noOfBook.setText("");
			zip.setText("");
			fname.setText("");
			lname.setText("");
			phone.setText("");
			street.setText("");
			city.setText("");
			state.setText("");
			bio.setText("");
			cred.setSelected(false);
		}


}
