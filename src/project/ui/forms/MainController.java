package project.ui.forms;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import project.business.*;

public class MainController {

	//CHECKRECORD
	@FXML
	private TextField tf_memberID;

	@FXML
	private TextField tf_ISBN;

	@FXML
	private TableColumn isbn1;

	@FXML
	private TableColumn title1;

	@FXML
	private TableColumn author1;

	@FXML
	private TableColumn noOfAvailable;

	@FXML
	private TableColumn checkoutlist;

	@FXML
	private Label lbl_fName;

	@FXML
	private Label lbl_phone;

	@FXML
	private Label lbl_cityState;
	@FXML
	private Label lbl_street;

	public void btn_search(ActionEvent event){
		
		Librarian n=new Librarian();
		LibraryMembers lm= n.getMemberInfo(tf_memberID.getText());
		//if available
		String s= lm.getFname()+lm.getLname();
		lbl_fName.setText(s);
		lbl_phone.setText(lm.getPhoneNumber());
		PersonAddress pa= lm.getAddress();
		lbl_cityState.setText(pa.getCity()+", "+pa.getState());
		lbl_street.setText(pa.getStreet());


	}



}
