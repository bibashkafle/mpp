package project.ui.forms;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import project.business.Administrator;
import project.business.LibraryMembers;
import project.business.PersonAddress;
import project.dataaccess.MemberDataAccess;


public class EditMemberController {
	@FXML
	private TextField txtFName;
	@FXML
	private TextField txtLName;
	@FXML
	private TextField txtMemId;
	@FXML
	private TextField txtPhone;
	@FXML
	private TextField txtStreet;
	@FXML
	private TextField txtCity;
	@FXML
	private TextField txtState;
	@FXML
	private TextField txtZip;

	LibraryMembers libMem;

	public void btnSearchMember_click(ActionEvent event) {
		try {
			System.out.println(txtMemId.getText());
			libMem = new Administrator().editMember(txtMemId.getText());
			if (libMem == null) {
				clear();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information");
				alert.setHeaderText(null);
				alert.setContentText("Member doesn't exist!");
				alert.showAndWait();
			} else {
				PersonAddress per = libMem.getAddress();
				txtFName.setText(libMem.getFname());
				txtLName.setText(libMem.getLname());
				txtPhone.setText(libMem.getPhoneNumber());
				txtStreet.setText(per.getStreet());
				txtCity.setText(per.getCity());
				txtState.setText(per.getState());
				txtZip.setText(per.getZip());
			}
		} catch (Exception e) {

		}
	}

	public void btnEditMember_click(ActionEvent event) {
		try {
			// LibraryMembers libraryMembers = new
			// LibraryMembers(txtFName.getText(), txtLName.getText(),
			// txtPhone.getText(), txtStreet.getText(), txtCity.getText(),
			// txtState.getText(),txtZip.getText(), txtMemId.getText());

			PersonAddress per = libMem.getAddress();
			libMem.setFname(txtFName.getText());
			libMem.setLname(txtLName.getText());
			libMem.setPhoneNumber(txtPhone.getText());
			libMem.setMemberId(txtMemId.getText());
			per.setStreet(txtStreet.getText());
			per.setCity(txtCity.getText());
			per.setState(txtState.getText());
			per.setZip(txtZip.getText());

			clear();
			MemberDataAccess memberDataAccess = new MemberDataAccess();
			memberDataAccess.EditUser(libMem);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Member is successfully edited");
			alert.showAndWait();
		} catch (Exception e) {

		}
	}

	public void clear() {
		txtFName.clear();
		txtLName.clear();
		txtMemId.clear();
		txtPhone.clear();
		txtStreet.clear();
		txtCity.clear();
		txtState.clear();
		txtZip.clear();
	}
}
