package project.ui.forms;

import javafx.event.ActionEvent;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import project.business.LibraryMembers;
import project.dataaccess.MemberDataAccess;


public class AddUserController {
@FXML
private TextField txtFName;
@FXML
private TextField txtLName;
@FXML
private TextField txtMemId;
@FXML
private TextField txtPhone;
@FXML
private TextField txtStreet;
@FXML
private TextField txtCity;
@FXML
private TextField txtState;
@FXML
private TextField txtZip;



public void btnAddMember_click(ActionEvent event) {
	try{
	LibraryMembers libraryMembers = new LibraryMembers(txtFName.getText(), txtLName.getText(), txtPhone.getText(), txtStreet.getText(), txtCity.getText(), txtState.getText(),txtZip.getText(), txtMemId.getText());
	clear();
	MemberDataAccess memberDataAccess = new MemberDataAccess();
	memberDataAccess.saveUser(libraryMembers);
	Alert alert = new Alert(AlertType.INFORMATION);
	alert.setTitle("Information");
	alert.setHeaderText(null);
	alert.setContentText("Member is successfully added");
	alert.showAndWait();
	}catch(Exception e){


						}
}

public void clear(){
	txtFName.clear();
	txtLName.clear();
	txtMemId.clear();
	txtPhone.clear();
	txtStreet.clear();
	txtCity.clear();
	txtState.clear();
	txtZip.clear();
}
}

