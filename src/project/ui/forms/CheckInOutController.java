package project.ui.forms;

import java.time.LocalDate;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import project.business.Author;
import project.business.Book;
import project.business.CheckOutEntry;
import project.business.CopyOfBooks;
import project.business.Librarian;
import project.business.LibraryMembers;
import project.business.PersonAddress;
import project.dataaccess.MemberDataAccess;

public class CheckInOutController {
	
	Librarian librarian;
	@FXML Label lbl_fName; 
	@FXML Label lbl_street;
	@FXML Label lbl_cityState;
	@FXML Label lbl_phoneNo;
	
	@FXML TableView dataGrid;
	@FXML private TextField txtMemberId;
	@FXML private TextField txtIsbn;
	
	TableColumn bookIsbn = new TableColumn("ISBN");
    TableColumn bookTitle = new TableColumn("Book Title");
    //TableColumn noOfBooks = new TableColumn("Total Book");
    TableColumn noOfAvailable = new TableColumn("Total Available");
    
    TableColumn bookId = new TableColumn("Book ID");
    TableColumn dateOfCheckOut = new TableColumn("Checkout Date");
    TableColumn returnedDate = new TableColumn("Dew Date");
   // TableColumn bookAuthor = new TableColumn("Author");
	
	
	public CheckInOutController() {
		// TODO Auto-generated constructor stub
		librarian = new Librarian();
	}
	
	
	
	@FXML protected void btnCheckOut_Click(ActionEvent event){
		try{
			Book book = librarian.getBookInfo(txtIsbn.getText());
			LibraryMembers member = librarian.getMemberInfo(txtMemberId.getText());
			librarian.CheckOutBooks(book, member);
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Book checkout successful");
			alert.showAndWait();
		}catch(Exception e)
		{


		}
	}
	
	@FXML protected void btnCheckIn_click(ActionEvent event){
		//librarian.CheckOutBooks();
	}
	
	@FXML protected void btnSearch_click(ActionEvent event){
		if(txtMemberId.getText().isEmpty())
		{
			System.out.println("Member id and isbn no required");
			return;
		}
		
		
		LibraryMembers member =  librarian.getMemberInfo(txtMemberId.getText());
		//lbl_fName.setText("You click this");
		lbl_fName.setText(member.getFname()+" "+member.getLname());
		lbl_phoneNo.setText(member.getPhoneNumber());
		PersonAddress address = member.getAddress();
		lbl_street.setText(address.getStreet());
		lbl_cityState.setText(address.getCity()+","+address.getState());
		
		if(!txtIsbn.getText().isEmpty()){
			Book book = librarian.getBookInfo(txtIsbn.getText());
		
		     ObservableList<Book> data = FXCollections.observableArrayList(book);
		         
			 bookIsbn.setCellValueFactory(new PropertyValueFactory<Book, String>("ISBN"));
			 bookTitle.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
			 //noOfBooks.setCellValueFactory(new PropertyValueFactory<Book, String>("noOfBooks"));
			 noOfAvailable.setCellValueFactory(new PropertyValueFactory<Book, String>("noOfAvailableBooks"));
			 
			 dataGrid.setItems(data);
			 dataGrid.getColumns().addAll(bookIsbn, bookTitle,noOfAvailable);
			
		}
		else{
			List<CheckOutEntry> list = librarian.getCheckOutEntry(txtMemberId.getText());
		
			ObservableList<CheckOutEntry> data = FXCollections.observableArrayList(list);
			 bookIsbn.setCellValueFactory(new PropertyValueFactory<CheckOutEntry, String>("Isbn"));
			 bookId.setCellValueFactory(new PropertyValueFactory<CheckOutEntry, String>("bookId"));
			 
			 dateOfCheckOut.setCellValueFactory(new PropertyValueFactory<CheckOutEntry, String>("dateOfCheckOut"));
			 returnedDate.setCellValueFactory(new PropertyValueFactory<CheckOutEntry, String>("returnedDate"));
			 
			 dataGrid.setItems(data);
			 dataGrid.getColumns().addAll(bookIsbn, bookId,dateOfCheckOut,returnedDate);
			
		}
	}
}
