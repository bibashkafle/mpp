package project.ui;

public class Forms {
	public static final String LOGIN = "../ui/forms/LoginForm.fxml";
	public static final String MAINFORM = "../ui/forms/MainForm.fxml";
	public static final String ADDBOOK = "../ui/forms/Addbook.fxml";
	public static final String ADDMEMBER = "../ui/forms/AddMember.fxml";
	public static final String CHECKINOUT = "../ui/forms/CheckInOut.fxml";
	public static final String HOMEPAGE = "../ui/forms/homepage.fxml";
	public static final String ADDBOOKCOPY = "../ui/forms/AddBookCopy.fxml";
	public static final String EDITMEMBER = "../ui/forms/EditMember.fxml";
	
	/*public static void main(String[] args){
		System.out.println(System.getProperty("user.dir"));
	}*/
}
