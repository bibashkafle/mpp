package project.business;

import java.io.Serializable;

public class Author extends Person implements Serializable  {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	@Override
	public String toString() {
		return "Author [credentials=" + credentials + ", shortbio=" + shortbio + "]";
	}
	boolean credentials;
	private String shortbio;
	public Author(String fname, String lname, String phoneNumber, String street, String city, String state, String zip,
			boolean credentials, String shortbio) {
		super(fname, lname, phoneNumber, street, city, state, zip);
		this.credentials = credentials;
		this.shortbio = shortbio;
	}
	public boolean isCredentials() {
		return credentials;
	}
	public void setCredentials(boolean credentials) {
		this.credentials = credentials;
	}
	public String getShortbio() {
		return shortbio;
	}
	public void setShortbio(String shortbio) {
		this.shortbio = shortbio;
	}

}
