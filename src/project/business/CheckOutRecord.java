package project.business;

import java.util.ArrayList;
import java.util.List;
	public abstract class CheckOutRecord {

		private List<CheckOutEntry> entry=new ArrayList<CheckOutEntry>();
		private String record;
		LibraryMembers mem;
		public CheckOutRecord(String record) {

			this.record = record;
		}


		//public CheckOutEntry
		public abstract void recordOfFines();
		public abstract void datesPaid();


}


