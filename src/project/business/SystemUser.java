package project.business;

import java.io.Serializable;
import java.util.List;

import project.dataaccess.DataAccess;
import project.utils.UserRole;

public class SystemUser implements Serializable {
	
	private static final long serialVersionUID = 8309080721495266420L;
	private String userName;
	private String password;
	private UserRole role;
	
	public SystemUser(){
		
	}
	public SystemUser(String userName, String password, UserRole role) {
		this.userName = userName;
		this.password = password;
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}

	public UserRole getRole() {
		return role;
	}
	
	public SystemUser Login(String uname, String pwd){
		if(uname.isEmpty() || pwd.isEmpty())
			return null;

		List<SystemUser> list =  new DataAccess().Read("SystemUser");
		SystemUser userInfo = null;
		for (SystemUser systemUser : list) {			
			if(systemUser.getUserName().trim().equals(uname) && systemUser.getPassword().trim().equals(pwd)){
				userInfo = new SystemUser(systemUser.getUserName(),systemUser.getPassword(), systemUser.getRole());
				break;
			}
		}
		return userInfo;
	}

	@Override
	public String toString() {
		return "SystemUser [userName=" + userName + ", password=" + password + ", role=" + role + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemUser other = (SystemUser) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role != other.role)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}
	
	
	
}