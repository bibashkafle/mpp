package project.business;

import java.io.Serializable;

public class Person implements Serializable {

	@Override
	public String toString() {
		return "fname=" + fname + ", lname=" + lname + ", phoneNumber=" + phoneNumber + ", address=" + address;
	}
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String fname;
	private String lname;
	private String phoneNumber;
    private PersonAddress address;
	public Person(String fname, String lname, String phoneNumber, String street, String city, String state, String zip) {

		this.fname = fname;
		this.lname = lname;
		this.phoneNumber = phoneNumber;
		address = new PersonAddress(street,city,state,zip);
	}

	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public PersonAddress getAddress() {
		return address;
	}
	public void setAddress(PersonAddress address) {
		this.address = address;
	}


}
