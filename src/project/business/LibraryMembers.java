package project.business;

import java.io.Serializable;

public class LibraryMembers extends Person implements Serializable {


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String memberId;
	public LibraryMembers(String fname, String lname, String phoneNumber, String street, String city, String state,
			String zip, String memberId) {
		super(fname, lname, phoneNumber, street, city, state, zip);
		this.memberId= memberId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public LibraryMembers getMember (String membId) {
		return null;
	}

}
