package project.business;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckOutEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8688562595985254970L;
	private String Isbn;
	private String bookId;
	private LocalDate dateOfCheckOut;
	//private Date dueTo;
	private LocalDate returnedDate;
	private String status;
	//private List<CheckOutRecord> listOfCheckOuts = new ArrayList<CheckOutRecord>();
	private LibraryMembers member;
	
	public CheckOutEntry(String isbn, String bookId, LocalDate dateOfCheckOut, LocalDate returnedDate, String status,
			LibraryMembers member) {
		super();
		Isbn = isbn;
		this.bookId = bookId;
		this.dateOfCheckOut = dateOfCheckOut;
		this.returnedDate = returnedDate;
		this.status = status;
		this.member = member;
	}
	public String getIsbn() {
		return Isbn;
	}
	public void setIsbn(String isbn) {
		Isbn = isbn;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public LocalDate getDateOfCheckOut() {
		return dateOfCheckOut;
	}
	public void setDateOfCheckOut(LocalDate dateOfCheckOut) {
		this.dateOfCheckOut = dateOfCheckOut;
	}
	public LocalDate getReturnedDate() {
		return returnedDate;
	}
	public void setReturnedDate(LocalDate returnedDate) {
		this.returnedDate = returnedDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LibraryMembers getMember() {
		return member;
	}
	public void setMember(LibraryMembers member) {
		this.member = member;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Isbn == null) ? 0 : Isbn.hashCode());
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		result = prime * result + ((dateOfCheckOut == null) ? 0 : dateOfCheckOut.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		result = prime * result + ((returnedDate == null) ? 0 : returnedDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckOutEntry other = (CheckOutEntry) obj;
		if (Isbn == null) {
			if (other.Isbn != null)
				return false;
		} else if (!Isbn.equals(other.Isbn))
			return false;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		if (dateOfCheckOut == null) {
			if (other.dateOfCheckOut != null)
				return false;
		} else if (!dateOfCheckOut.equals(other.dateOfCheckOut))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		if (returnedDate == null) {
			if (other.returnedDate != null)
				return false;
		} else if (!returnedDate.equals(other.returnedDate))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CheckOutEntry [Isbn=" + Isbn + ", bookId=" + bookId + ", dateOfCheckOut=" + dateOfCheckOut
				+ ", returnedDate=" + returnedDate + ", status=" + status + ", member=" + member + "]";
	}
	
}
