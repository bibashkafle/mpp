package project.business;

import java.io.Serializable;

public class CopyOfBooks implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1870601371528225263L;
	private boolean isAvailable;
	private int bookNo;
	private String isbn;
	
	public CopyOfBooks(){
		
	}
	
	public CopyOfBooks(String isbn, int bookNo, boolean isAvailable) {
		this.isbn = isbn;
		this.bookNo = bookNo;
		this.isAvailable = isAvailable;
	}

	public boolean isAvailable() {
		return isAvailable;
	}

	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public int getBookNo() {
		return bookNo;
	}
	
	public String getISBN() {
		return this.isbn;
	}
	
	public void setBookNo(int bookNo) {
		this.bookNo = bookNo;
	}

	@Override
	public String toString() {
		return "CopyOfBooks [isAvailable=" + isAvailable + ", bookNo=" + bookNo + ", isbn=" + isbn + "]";
	}	
	
}
