package project.business;

import java.util.List;

import project.dataaccess.DataAccess;

import java.io.Serializable;
import java.util.ArrayList;

public class Book implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String title;
	private String ISBN;
	private List<Author> aut =new ArrayList<Author>();
	private String noOfBooks;
	private String noOfAvailableBooks;

	public Book(String title, String iSBN, String noOfBook,String fname, String lname, String phoneNumber, String street, String city, String state, String zip,
		boolean credentials, String shortbio) {
		this.title = title;
		ISBN = iSBN;
		noOfBooks = noOfBook;
		noOfAvailableBooks = noOfBook;
		aut.add(new Author(fname, lname, phoneNumber, street, city, state, zip,credentials,shortbio));
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public List<Author> getAut() {
		return aut;
	}
	public void setAut(Author au) {
		aut.add(au);
	}
	public String getNoOfBook() {
		return noOfBooks;
	}
	public void setNoOfBook(String noOfBook) {
		this.noOfBooks = noOfBook;
	}
	public String getNoOfAvailableBooks() {
		return noOfAvailableBooks;
	}
	public void setNoOfAvailableBooks(String noOfAvailableBooks) {
		this.noOfAvailableBooks = noOfAvailableBooks;
	}

	public Book getBook(int iSBN){
		return null;
	}
	@Override
	public String toString() {
		return "Book [title=" + title + ", ISBN=" + ISBN + ", aut=" + aut + ", noOfBooks=" + noOfBooks
				+ ", noOfAvailableBooks=" + noOfAvailableBooks + "]";
	}

}
