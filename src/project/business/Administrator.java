package project.business;

import java.util.ArrayList;
import java.util.List;

import project.dataaccess.DataAccess;

public class Administrator extends DataAccess {

	private List<Book> listOfBook;
	private List<LibraryMembers> libMem = new ArrayList();
	private LibraryMembers libmem;

	/*public void addMembers(String fname, String lname, String phoneNumber, String street, String city, String state, String zip, String memberId) {
		libmem = new LibraryMembers(fname, lname, phoneNumber, street, city, state, zip, memberId);
		libMem.add(libmem);
	}*/

	public LibraryMembers editMember(String text) {
		libMem =  Read("LibraryMembers");
		for(LibraryMembers libMember:libMem){
			if(libMember.getMemberId().equals(text))
				return libMember;
			}

		return null;
	}

	public void addBook(String title, String iSBN, String noOfBook, String fname, String lname, String phoneNumber,	String street, String city, String state, String zip, boolean credentials, String shortbio) {

		List<CopyOfBooks> copyOfBooks = new ArrayList<>();
		copyOfBooks = Read("CopyOfBooks");
		
		for (int i = 0; i < Integer.parseInt(noOfBook); i++) {
			copyOfBooks.add(new CopyOfBooks(iSBN, i, true));
		}

		//System.out.println(copyOfBooks);
		Write(copyOfBooks, "CopyOfBooks");

		listOfBook = new ArrayList<>();
		listOfBook = Read("Book");
		listOfBook.add(new Book(title, iSBN, noOfBook, fname, lname, phoneNumber, street, city, state, zip, credentials,shortbio));
		//System.out.println(listOfBook);

		Write(listOfBook, "Book");
	}
}
