package project.business;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import project.dataaccess.DataAccess;

public class Librarian extends DataAccess {


	public void CheckOutBooks(Book book, LibraryMembers member){
		List<CheckOutEntry> list = Read("CheckOutEntry");
		LocalDate RetrunDate = LocalDate.now().plusDays(7);
		int bookNo = 0;
		List<CopyOfBooks> bookList = Read("CopyOfBooks");
		for (CopyOfBooks copyOfBooks : bookList) {
			if(copyOfBooks.isAvailable() && copyOfBooks.getISBN().equals(book.getISBN())){
				bookNo = copyOfBooks.getBookNo();
				copyOfBooks.setAvailable(false);
			}
		}
		
		List<Book> books  = Read("Book");
		
		for (Book bks : books) {
			if(bks.getISBN().equals(book.getISBN())){
				int totalBook = Integer.parseInt(bks.getNoOfAvailableBooks());
				bks.setNoOfAvailableBooks(String.valueOf(totalBook-1));
			}
		}
		
		list.add(new CheckOutEntry(book.getISBN(), String.valueOf(bookNo), LocalDate.now(), RetrunDate, "Checkout", member));
		
		Write(books, "Book");
		Write(list, "CheckOutEntry");
		Write(bookList, "CopyOfBooks");
	}

	public void CheckInBooks(CheckOutEntry entry){
			
	}

	public LibraryMembers getMemberInfo(String memId){
		List<LibraryMembers> list = Read("LibraryMembers");
		//System.out.println(list);
		for (LibraryMembers libraryMembers : list) {
			if(libraryMembers.getMemberId().equals(memId))
				return libraryMembers;
		}
		return null;
	}
	
	public Book getBookInfo(String ISBN){
		List<Book> list = Read("Book");
		//System.out.println(list);
		for (Book book : list) {
			if(book.getISBN().equals(ISBN) && Integer.parseInt(book.getNoOfAvailableBooks())>=1)
				return book;
		}
		
		return null;
	}
	
	public List<CheckOutEntry> getCheckOutEntry(String memId){
		List<CheckOutEntry> list = Read("CheckOutEntry");
		List<CheckOutEntry> entry = new ArrayList<>();
		for (CheckOutEntry checkOutEntry : list) {
			if(checkOutEntry.getMember().getMemberId().equals(memId))
				entry.add(checkOutEntry);
		}
		return entry;
	}
}
