package project.dataaccess;

import java.util.ArrayList;
import java.util.List;


import project.business.LibraryMembers;

public class MemberDataAccess extends DataAccess{
	public void saveUser(LibraryMembers members){
		List<LibraryMembers> libraryMembers = Read("LibraryMembers");
		libraryMembers.add(members);
		Write(libraryMembers, "LibraryMembers");
	}
	
	public void EditUser(LibraryMembers members){
		List<LibraryMembers> libraryMembers = Read("LibraryMembers");
		for (LibraryMembers mlist : libraryMembers) {
			if(mlist.getMemberId().equals(members.getMemberId())){
				libraryMembers.remove(mlist);
				break;
			}
		}
			libraryMembers.add(members);
			Write(libraryMembers, "LibraryMembers");
	}
}








