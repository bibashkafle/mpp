package project.dataaccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import project.utils.Utilities;

public class DataAccess {
	private String OUTPUT_DIR ;
	
	public DataAccess(){
		OUTPUT_DIR = Utilities.GetRootDirectory()+ "\\src\\project\\storage\\";
	}
	
	public <T> Boolean Write(List<T> list, String classname){
		try{
				FileOutputStream fileOutputStream = new FileOutputStream(this.OUTPUT_DIR+classname);
				ObjectOutputStream output = new ObjectOutputStream(fileOutputStream);
				output.writeObject(list);
				output.close();
				return true;
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		return false;
	}
	
	public <T> List<T> Read(String classname){
		 ObjectInputStream input;
		ArrayList<T> allMembers = new ArrayList<T>();
		try {
			
            FileInputStream fileInputStream = new FileInputStream(new File(this.OUTPUT_DIR+classname));
             input = new ObjectInputStream(fileInputStream);
            allMembers = (ArrayList<T>) input.readObject();
            input.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			}
		
		return allMembers;
	}
	
}
