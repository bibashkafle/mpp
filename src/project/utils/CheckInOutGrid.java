package project.utils;

import java.io.Serializable;

public class CheckInOutGrid implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6332947907025006803L;
	private String title;
	private String ISBN;
	private String author;
	private String status;
	
	public CheckInOutGrid(String title, String iSBN, String author, String status) {
		super();
		this.title = title;
		ISBN = iSBN;
		this.author = author;
		this.status = status;
	}

	@Override
	public String toString() {
		return "CheckInOutGrid [title=" + title + ", ISBN=" + ISBN + ", author=" + author + ", status=" + status + "]";
	}
	
	
	
}
