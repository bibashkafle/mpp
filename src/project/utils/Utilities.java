package project.utils;

import java.text.Normalizer.Form;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import project.ui.Forms;

public class Utilities {

	public void openDashboardWindow(String windowName, String title, String role) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource(windowName));
			Stage stage = new Stage();
			stage.setTitle(title);
			stage.setResizable(false);
			stage.setScene(new Scene(root));
			if(windowName==Forms.HOMEPAGE){
				if(role=="ADMINSTRATOR"){
					root.lookup("#btnCheckOutBook").setDisable(true);
				}
				else if(role=="LIBRARION"){
					root.lookup("#btnAddMember").setDisable(true);
					root.lookup("#btnEditMember").setDisable(true);
					root.lookup("#btnAddBook").setDisable(true);
					root.lookup("#btnAddBookCopy").setDisable(true);
				}
			}
	
			stage.show();
			
		

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	


	public static String GetRootDirectory(){
		return System.getProperty("user.dir");
	}
}
