package  w1_l2_home_work.prob2c;

import java.util.ArrayList;
import java.util.List;

public class Student {
	private String name;
	private int id;
	private List<Section> section;
	
	public Student(){
		section = new ArrayList<>();
	}
	public Student(String name, int id) {
		this.name = name;
		this.id = id;
		section = new ArrayList<>();
	}
	
	public Student(String name, int id, int sectionNo) {
		this.name = name;
		this.id = id;
		section = new ArrayList<>();
		section.add(new Section(sectionNo,this));
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	  public List<Section> getSection() {
	        return section;
	    }
	  
	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + ", section=" + section + "]";
	}
	public void addSection(int sectionNum){
		section.add(new Section(sectionNum,this));
	}
}
