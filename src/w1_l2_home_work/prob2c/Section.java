package  w1_l2_home_work.prob2c;

import java.util.ArrayList;
import java.util.List;

public class Section {
	private int sectionNum;
	private List<Student> student;
	public Section(int sectionNum, Student student) {
		
		this.sectionNum = sectionNum;
		this.student = new ArrayList<>();
		this.student.add(student);
	}

	public int getSectionNum() {
		return sectionNum;
	}

	public void setSectionNum(int sectionNum) {
		this.sectionNum = sectionNum;
	}
	
	public void addStudent(Student student){
		this.student.add(student);
	}

	@Override
	public String toString() {
		return "Section [sectionNum=" + sectionNum + "]";
	}
	
}
