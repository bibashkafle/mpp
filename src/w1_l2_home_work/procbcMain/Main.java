package  w1_l2_home_work.procbcMain;
import java.util.*;
import w1_l2_home_work.prob2c.Student;
public class Main {
	
	public static void main(String[] args){
		List<Student> studentList = createStudentList();
		for (Student student : studentList) {
			System.out.println(student.toString());
		}
	}
	
	public static List<Student> createStudentList(){
		Student s1 = new Student("Bibash", 1);
		s1.addSection(101);
		Student s2 = new Student("Bizen", 2);
		s2.addSection(102);
		Student s3 = new Student("Subhechha", 2);
		s3.addSection(103);
		return Arrays.asList(s1, s2, s3);
	}
}

/* output
 *  Student [name=Bibash, id=1, section=[Section [sectionNum=101]]]
	Student [name=Bizen, id=2, section=[Section [sectionNum=102]]]
	Student [name=Subhechha, id=2, section=[Section [sectionNum=103]]]
 * */
