package  w1_l2_home_work.projectmanagement;

public class Feature {
	private int hourRequired;
	private int percentageRemaining;
	
	public int getHourRequired() {
		return hourRequired;
	}
	public void setHourRequired(int hourRequired) {
		this.hourRequired = hourRequired;
	}
	public int getPercentageRemaining() {
		return percentageRemaining;
	}
	public void setPercentageRemaining(int percentageRemaining) {
		this.percentageRemaining = percentageRemaining;
	}
	
}
