package  w1_l2_home_work.projectmanagement;

import java.util.List;

public class Project {
	
	private String projectId;
	private List<Feature> backlogFeatureList;
	private List<Release> releaseList;
	
	public Project() {
			
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public void addFeature(Feature fobj){
		backlogFeatureList.add(fobj);
	}
	
	public void addRealse(Release rObj){
		releaseList.add(rObj);
	}

}
