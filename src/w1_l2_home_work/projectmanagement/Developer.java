package  w1_l2_home_work.projectmanagement;

import java.util.List;

public class Developer {
	private String developerId;
	private List<Feature> assignedFeatures;
	public String getDeveloperId() {
		return developerId;
	}
	public void setDeveloperId(String developerId) {
		this.developerId = developerId;
	}
	public void assignFeature(Feature fObj){
		assignedFeatures.add(fObj);
	}

}
