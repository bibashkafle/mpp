package  w1_l2_home_work.prob2main;
import java.util.*;
import  w1_l2_home_work.prob2b.*;
public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Order> listOfStudents = createListOfOrder();
		//System.out.println(listOfStudents);
		for (Order order : listOfStudents) {
			System.out.println(order.toString());
		}	
	}
	
	private static List<Order> createListOfOrder(){
		Order o1 = new Order(1,"2-16-2016",1,10.1,1);
		Order o2 = new Order(1,"2-16-2016",2,11.2,2);
		Order o3 = new Order(1,"2-16-2016",3,12.3,3);
		Order o4 = new Order(1,"2-16-2016",4,13.4,4);
		Order o5 = new Order(1,"2-16-2016",5,14.5,5);
		return Arrays.asList(o1, o2, o3, o4,o5);
	}

}

/* output
 * Order [orderNum=1, dateOfOrder=2016-02-16, orderLine=[OrderLine [lineNo=1, price=10.1, quantity=1]]]
Order [orderNum=1, dateOfOrder=2016-02-16, orderLine=[OrderLine [lineNo=2, price=11.2, quantity=2]]]
Order [orderNum=1, dateOfOrder=2016-02-16, orderLine=[OrderLine [lineNo=3, price=12.3, quantity=3]]]
Order [orderNum=1, dateOfOrder=2016-02-16, orderLine=[OrderLine [lineNo=4, price=13.4, quantity=4]]]
Order [orderNum=1, dateOfOrder=2016-02-16, orderLine=[OrderLine [lineNo=5, price=14.5, quantity=5]]]
*/
