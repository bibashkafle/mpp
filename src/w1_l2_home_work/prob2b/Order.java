package w1_l2_home_work.prob2b;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Order {
	private int orderNum;
	private LocalDate dateOfOrder;
	 private List<OrderLine> orderLine;
	 
	 /*public Order(){
		 orderLine = new ArrayList<>();
	 }*/
    public Order(int orderNum, String dateOfOrder,int lineNo, double price, int quantity) {
		this.orderNum = orderNum;
		this.dateOfOrder = LocalDate.parse(dateOfOrder, DateTimeFormatter.ofPattern("M-d-yyyy"));
		orderLine = new ArrayList<>();
		orderLine.add(new OrderLine(lineNo, price, quantity,this));
	}
    
	public int getOrderNum() {
		return orderNum;
	}
	
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	
	public LocalDate getDateOfOrder() {
		return dateOfOrder;
	}
	
	public void setDateOfOrder(LocalDate dateOfOrder) {
		this.dateOfOrder = dateOfOrder;
	}
	
	
	public List<OrderLine> getOrderLine() {
        return orderLine;
    }

    public void addOrderLine(OrderLine orderLine) {
        this.orderLine.add(orderLine);
    }

	@Override
	public String toString() {
		return "Order [orderNum=" + orderNum + ", dateOfOrder=" + dateOfOrder + ", orderLine=" + orderLine + "]";
	}
    
    
	
}
