package  w1_l2_home_work.prob2b;

public class OrderLine {
	
	private int lineNo;
	private double price;
	private int quantity;
	private Order order;
   
	public OrderLine(int lineNo, double price, int quantity, Order order) {
		this.lineNo = lineNo;
		this.price = price;
		this.quantity = quantity;
		this.order = order;
	}
	
	public int getLineNo() {
		return lineNo;
	}
	
	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "OrderLine [lineNo=" + lineNo + ", price=" + price + ", quantity=" + quantity + "]";
	}
}
