package w1_l3_homework.problem_2;

import java.util.ArrayList;
import java.util.List;

public class Buildings extends LandLord{
	
	private String buildingName;
	private double maintenanceCost;
	//private LandLord landLord;
	private List<Apartments> apartments;
	
	public Buildings(String landLordName, String buildingName, double maintenanceCost) {
		//landLord = new LandLord(landLordName);
		super(landLordName);
		this.buildingName = buildingName;
		this.maintenanceCost = maintenanceCost;
		apartments = new ArrayList<>();
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public double getMaintenanceCost() {
		return maintenanceCost;
	}

	public void setMaintenanceCost(double maintenanceCost) {
		this.maintenanceCost = maintenanceCost;
	}
	
	public void setLandLord(String landLordName){
		super.setLandLordName(landLordName);
	}
	
	public String getLandLord(){
		return super.getLandLordName();
	}
	
	public void setAppartment(int apartmentId, double rentAmount){
		apartments.add(new Apartments(apartmentId, rentAmount,this));
	}
	
	public List<Apartments> getAppartment(){
		return apartments;
	}
	
	public double getTotalRentAmout(){
		double totalRent = 0;
		for (Apartments flat : apartments) {
			totalRent+= flat.getRentAmount();
		}
		return totalRent;
	}
	
	public double getProfit(){
		return this.getTotalRentAmout()-this.maintenanceCost;
	}
	
	public double getMonthlyProfit(){
		return ((this.getTotalRentAmout()-this.maintenanceCost)/12);
	}
	
	@Override
	public String toString() {
		return "\n\nBuildings [buildingName=" + buildingName + ",\n maintenanceCost=" + maintenanceCost + ",\n totalRentAmount="
				+ this.getTotalRentAmout() + ",\n total profit=" + this.getProfit() + ",\n Monthly profit=" + this.getMonthlyProfit() +"]";
	}
	
}
