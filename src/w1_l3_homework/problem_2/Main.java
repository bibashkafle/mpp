package w1_l3_homework.problem_2;
import java.util.*;

public class Main {
	
	public static void main(String[] args){
		Buildings bibashTower = new Buildings("Bibash", "Bibash tower", 1000);
		bibashTower.setAppartment(10, 2000);
		bibashTower.setAppartment(11, 2000);
		bibashTower.setAppartment(12, 2000);
		bibashTower.setAppartment(13, 2000);
		
		Buildings bizenTower = new Buildings("Bizen", "Bizen tower", 1100);
		bizenTower.setAppartment(14, 3000);
		bizenTower.setAppartment(15, 3000);
		bizenTower.setAppartment(16, 3000);
		bizenTower.setAppartment(17, 3000);
		
		Buildings subhechhaTower = new Buildings("Subhechha", "Subhechha tower", 1200);
		subhechhaTower.setAppartment(18, 4000);
		subhechhaTower.setAppartment(19, 4000);
		subhechhaTower.setAppartment(20, 4000);
		subhechhaTower.setAppartment(21, 4000);
		
		LandLord landLord[] = {bibashTower,bizenTower,subhechhaTower};
		
		for (LandLord tower : landLord) {
			System.out.println(tower.toString());
		}
	}
}

/**
 * 
 * 
 Buildings [buildingName=Bibash tower,
 maintenanceCost=1000.0,
 totalRentAmount=8000.0,
 total profit=7000.0,
 Monthly profit=583.3333333333334]

Buildings [buildingName=Bizen tower,
 maintenanceCost=1100.0,
 totalRentAmount=12000.0,
 total profit=10900.0,
 Monthly profit=908.3333333333334]

Buildings [buildingName=Subhechha tower,
 maintenanceCost=1200.0,
 totalRentAmount=16000.0,
 total profit=14800.0,
 Monthly profit=1233.3333333333333]

 */
