package w1_l3_homework.problem_2;

public class LandLord {
	private String landLordName;
	public LandLord(String landLordName){
		this.landLordName= landLordName;
	}
	
	
	public String getLandLordName() {
		return landLordName;
	}

	public void setLandLordName(String landLordName) {
		this.landLordName = landLordName;
	}


	@Override
	public String toString() {
		return "LandLord [landLordName=" + landLordName + "]";
	}
	
	

}
