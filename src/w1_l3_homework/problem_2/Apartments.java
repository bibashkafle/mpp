package w1_l3_homework.problem_2;

public class Apartments {
	private int apartmentId;
	private double rentAmount;
	private Buildings building;
	public Apartments(int apartmentId, double rentAmount, Buildings building) {
		this.apartmentId = apartmentId;
		this.rentAmount = rentAmount;
		this.building = building;
	}
	
	public int getApartmentId() {
		return apartmentId;
	}
	
	public void setApartmentId(int apartmentId) {
		this.apartmentId = apartmentId;
	}
	
	public double getRentAmount() {
		return rentAmount;
	}
	
	public void setRentAmount(double rentAmount) {
		this.rentAmount = rentAmount;
	}

	@Override
	public String toString() {
		return "Apartments [apartmentId=" + apartmentId + ", rentAmount=" + rentAmount + ", building=" + building.getBuildingName() + "]";
	}
	
}
