package w1_l3_homework.problem_3.problem_3a;

public class Cylinder extends Circle {
	private double height;
	
	public Cylinder(){
		
	}
	
	public Cylinder(double radius, double height){
		super(radius);
		this.height= 1.0;
	}
	public Cylinder(double height) {
		this.height = height;
	}

	public double getHeight() {
		return height;
	}

	public double getVolume() {
		return getArea()*height;
	}
	

}
