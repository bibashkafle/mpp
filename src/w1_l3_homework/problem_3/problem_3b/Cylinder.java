package w1_l3_homework.problem_3.problem_3b;

public class Cylinder {
	private Circle cylinderBase;
   private double height;
	
	public Cylinder(){
		
	}
	
	public Cylinder(double radius, double height) {
		this.height = 1.0;
		cylinderBase= new Circle(radius);
	}

	public double getHeight() {
		return height;
	}

	public double getVolume() {
		return cylinderBase.getArea()*height;
	}
	
}
