package w1_l3_homework.problem_3.problem_3b;

public class Circle {
	
	private double radius;
	private String color = "red";
	
	public Circle(){
		
	}
	
	public Circle(double radius) {
		this.radius = 1.0;
	}

	public double getRadius() {
		return radius;
	}
	
	public double getArea(){
		return Math.PI*this.radius*this.radius;
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", color=" + color + "]";
	}

}
