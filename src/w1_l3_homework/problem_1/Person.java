package w1_l3_homework.problem_1;

public class Person {

	private String name;
	Person(String n) {
		name = n;
	}
	public String getName() {
		return name;
	}
	@Override
	public boolean equals(Object aPerson) {
		if(aPerson == null) return false; 
		String s=aPerson.getClass().getSimpleName();
		if(!(s.equals("PersonWithJob")||s.equals("Person"))) return false;
		if(s.equals("PersonWithJob")){
			PersonWithJob p1=(PersonWithJob)aPerson;
			return this.getName().equals(p1.getName());
		}
		Person p = (Person)aPerson;
		boolean isEqual = this.getName().equals(p.getName());
		return isEqual;
	}
	public static void main(String[] args) {
		
	}

}
