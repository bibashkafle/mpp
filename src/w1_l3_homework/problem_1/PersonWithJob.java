package w1_l3_homework.problem_1;

public class PersonWithJob {
	
	private double salary;
	private Person person;
	public double getSalary() {
		return salary;
	}
	public String getName(){
		return person.getName();
	}
	PersonWithJob(String n, double s) {
		person=new Person(n);
		salary = s;
	}
	
	@Override
	public boolean equals(Object aPerson) {
		if(aPerson == null) return false; 
		String s=aPerson.getClass().getSimpleName();
		if(!(s.equals("PersonWithJob")||s.equals("Person"))) return false;
		if(s.equals("Person")){
			Person p1=(Person)aPerson;
			return this.getName().equals(p1.getName());
		}
		PersonWithJob p = (PersonWithJob)aPerson;
		boolean isEqual = this.getName().equals(p.getName()) &&
				this.getSalary()==p.getSalary();
		return isEqual;
	}
	public static void main(String[] args) {
		PersonWithJob p1 = new PersonWithJob("Joe", 30000);
		Person p2 = new Person("Joe");
		//As PersonsWithJobs, p1 should be equal to p2
		System.out.println("p1.equals(p2)? " + p1.equals(p2));
		System.out.println("p2.equals(p1)? " + p2.equals(p1));
	}


}

/* In the previous code the comparison was resulting to different results because the inheritance was violating the Liskov Substitution principle
 which results fragile in code.
 In this code we try to overcome the problem by using composition in stead of inheritance, which allow as to reuse the Person class code but also prevent
 the code from failing.
 *****out put ****
 
 p1.equals(p2)? true
 p2.equals(p1)? true

*/