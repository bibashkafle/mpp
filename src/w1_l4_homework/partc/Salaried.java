package w1_l4_homework.partc;

public class Salaried extends Employee{

	private double Salary;
	public Salaried(int empId, double salary) {
		super(empId);
		Salary = salary;
	}
	
	
	public double getSalary() {
		return Salary;
	}


	public void setSalary(double salary) {
		Salary = salary;
	}


	@Override
	double clacGrossPay(int month, int year) {
		// TODO Auto-generated method stub
		return this.Salary;
	}

}
