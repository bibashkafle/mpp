package w1_l4_homework.partc;

public class Hourly extends Employee {
	
	private double hourlyWage;
	private int hoursPerWeek;
	
	
	public Hourly(int empId, double hourlyWage, int hoursPerWeek) {
		super(empId);
		this.hourlyWage = hourlyWage;
		this.hoursPerWeek = hoursPerWeek;
	}


	public double getHourlyWage() {
		return hourlyWage;
	}


	public void setHourlyWage(double hourlyWage) {
		this.hourlyWage = hourlyWage;
	}


	public int getHoursPerWeek() {
		return hoursPerWeek;
	}


	public void setHoursPerWeek(int hoursPerWeek) {
		this.hoursPerWeek = hoursPerWeek;
	}


	@Override
	double clacGrossPay(int month, int year) {
		// TODO Auto-generated method stub
		return this.hourlyWage*this.hoursPerWeek;
	}

}
