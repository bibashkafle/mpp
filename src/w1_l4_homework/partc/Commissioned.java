package w1_l4_homework.partc;

import java.util.*;

public class Commissioned extends Employee{
	private double commission;
	private double baseSalary;
	
	private List<Order> orderList; 
	
	public Commissioned(int empId, double commission, double baseSalary) {
		super(empId);
		this.commission = commission;
		this.baseSalary = baseSalary;
	}
	

	public double getCommission() {
		return commission;
	}


	public void setCommission(double commission) {
		this.commission = commission;
	}


	public double getBaseSalary() {
		return baseSalary;
	}
	
	public void addOrder(Order o){
		orderList = new ArrayList<>();
		orderList.add(o);
	}
	
	public List<Order> getOrder(){
		return orderList;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}


	@Override
	double clacGrossPay(int month, int year) {
		// TODO Auto-generated method stub
		double totalOrderAmount = 0;
		for (Order order : orderList) {
			//if(order.getOrderDate().getMonth().equals(month))
			/*System.out.println("Month: "+order.getOrderDate().getMonthValue());
			System.out.println("Year: "+order.getOrderDate().getYear());*/
			
			if(order.getOrderDate().getMonthValue() == month && order.getOrderDate().getYear() == year)
				totalOrderAmount+=order.getOrderAmount();
			
		}
		return ((totalOrderAmount*getCommission())/100)+this.getBaseSalary();
	}

}
