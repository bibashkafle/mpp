package w1_l4_homework.partc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Order {
	private int orderNo;
	private LocalDate orderDate;
	private int orderAmount;
	
	public Order(int orderNo, String orderDate, int orderAmount) {
		this.orderNo = orderNo;
		this.orderDate = LocalDate.parse(orderDate, DateTimeFormatter.ofPattern("M-d-yyyy"));
		this.orderAmount = orderAmount;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = LocalDate.parse(orderDate, DateTimeFormatter.ofPattern("M-d-yyyy"));
	}

	public int getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}
	
	

}
