package w1_l4_homework.partc;

public class Paycheck {
	
	double grossPay = 0;
	
	final double fica = 23; 
	final double state = 5; 
	final double local = 1;
	final double medicare = 3; 
	final double socialSecurity = 7.5;
	
	public Paycheck(double grossPay) {
		this.grossPay = grossPay;
	}

	public void print(){
		double netSalary = this.getNetPay();
		System.out.println("Net salary: "+netSalary);
	}
	
	public double getNetPay(){
		
		double fica_deduct = (this.grossPay * this.fica)/100;
		double state_deduct = (this.grossPay * this.state)/100;
		double local_deduct = (this.grossPay * this.local)/100;
		double medicare_deduct = (this.grossPay * this.medicare)/100;
		double socialSecurity_deduct = (this.grossPay * this.socialSecurity)/100;
		
		return this.grossPay - fica_deduct - state_deduct - local_deduct - medicare_deduct - socialSecurity_deduct;
	}

}
