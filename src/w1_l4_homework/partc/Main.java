package w1_l4_homework.partc;

public class Main {
	
	
	public static void main(String[] args){
		Employee hourly = new Hourly(90, 10, 20);
		hourly.print(0, 0);
		
		Employee salaried = new Salaried(91, 19000);
		salaried.print(0, 0);
		
		Employee commission = new Commissioned(92, 10, 2000);
		commission.addOrder(new Order(10, "3-20-2012", 3000));
		commission.addOrder(new Order(11, "3-21-2012", 4000));
		commission.addOrder(new Order(12, "3-22-2012", 5000));
		commission.addOrder(new Order(13, "3-23-2012", 6000));
		commission.print(3, 2012);
	}
	
}
