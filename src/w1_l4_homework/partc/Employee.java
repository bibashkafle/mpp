package w1_l4_homework.partc;

public abstract class Employee {
	private int empId;
	
	public Employee(int empId) {
		this.empId = empId;
	}

	public void print(int month, int year){
		this.calCompensation(month, year).print();
	}
	
	abstract double clacGrossPay(int month, int year);
	public void addOrder(Order o){
			
	}
	
	public Paycheck calCompensation(int month, int year){
		double grossSalary = this.clacGrossPay(month,year);
		return new Paycheck(grossSalary);
	}
	
}
