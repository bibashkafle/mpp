package w1_l5_homework.problem_2;

public interface Figure {
	public double computeArea();
	
}
