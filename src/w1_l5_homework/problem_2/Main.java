package w1_l5_homework.problem_2;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double areaSum = 0.0;
		List<Figure> figureList = new ArrayList<Figure>();
		figureList.add(new Rectangle(5,5));
		figureList.add(new Triangle(3,5));
		figureList.add(new Circle(5));
		
		for(Figure f: figureList){
			areaSum+=f.computeArea();
		}
		System.out.println(areaSum);

	}
	
	

}
