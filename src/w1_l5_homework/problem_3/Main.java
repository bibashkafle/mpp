package w1_l5_homework.problem_3;

public class Main {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VehicleFactory vehicle = new VehicleFactory();
		vehicle.getVechicle("bus").StartEngine();
		vehicle.getVechicle("truck").StartEngine();
		vehicle.getVechicle("car").StartEngine();
		vehicle.getVechicle("electriccar").StartEngine();
	}
}
/* output

	Bus Started
	Truck started
	Car started
	Electric car started

 */
