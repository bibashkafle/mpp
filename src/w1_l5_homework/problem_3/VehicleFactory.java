package w1_l5_homework.problem_3;

public class VehicleFactory {
	
	public Vehicle getVechicle(String v){
		
		if(v.toLowerCase().equals("bus"))
			return new Bus();
		else if(v.toLowerCase().equals("truck"))
			return new Truck();
		else if(v.toLowerCase().equals("car"))
			return new Car();
		else if(v.toLowerCase().equals("electriccar"))
			return new ElectricCar();

		return null;
	}
}
