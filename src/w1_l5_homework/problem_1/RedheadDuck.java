package w1_l5_homework.problem_1;

public class RedheadDuck extends Duck{

	public RedheadDuck(){
		f=new FlyWithWings();
		q=new Quack();
		
	}
	public void display(){
		f.fly();
		q.quack();
	}
}
