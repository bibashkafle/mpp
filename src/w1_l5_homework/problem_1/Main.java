package w1_l5_homework.problem_1;

public class Main {

	public static void main(String[] args) {
		
		Duck[] ducks={new MallarDuck(), new DecoyDuck(), new RedheadDuck(), new RubberDuck()};
		for(Duck d:ducks){
			System.out.println();
			System.out.println(d.getClass().getSimpleName() + ":");
			d.display();
			d.swim();
		}

	}

}

/*
 * 
MallarDuck:
 fly with wings.
 quacking.
 swimming

DecoyDuck:
 cannot fly.
 cannot quack.
 swimming

RedheadDuck:
 fly with wings.
 quacking.
 swimming

RubberDuck:
 cannot fly.
 squeaking.
 swimming

 */
