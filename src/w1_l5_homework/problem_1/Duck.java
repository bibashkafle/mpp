package w1_l5_homework.problem_1;

public abstract class Duck {
 
	FlyBehavior f;
	QuackBehavior q;
	public void swim(){
		System.out.println(" swimming");
	}
	public abstract void display();
		
}
