package w1_l5_homework.problem_1;

public class DecoyDuck extends Duck{

	public DecoyDuck(){
		f=new CannotFly();
		q= new MuteQuack();
	}
	public void display(){
		f.fly();
		q.quack();
	}
}
