package w1_l5_homework.problem_1;

public class RubberDuck extends Duck{

	public RubberDuck(){
		f=new CannotFly();
		q= new Squeak();
	}
	public void display(){
		f.fly();
		q.quack();
	}
}

