package w1_l5_homework.problem_1;

public class MallarDuck extends Duck{

	public MallarDuck(){
		f=new FlyWithWings();
		q=new Quack();
	}
	public void display(){
		f.fly();
		q.quack();
	}
}

